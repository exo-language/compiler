#include <iostream>
#include <memory>
#include <unordered_map>
#include <string>
#include <functional>
#include <optional>

namespace exo {
    struct Object final : public std::enable_shared_from_this<Object> {
        Object(std::shared_ptr<Object const> parent):
            parent { parent }
        {}

        void emplace_property(std::string const& name, std::function<std::shared_ptr<Object const>()> property) {
            properties.emplace(name, [=, this]() {
                auto const&& evaluated_property { property() };
                properties.find(name)->second = [=]() { return evaluated_property; };
                return evaluated_property;
            });
        }

        std::shared_ptr<Object const> at(std::string const& name) const {
            auto const& property { properties.find(name) };
            if (property != properties.end()) return property->second();

            std::cerr << "cannot select undefined property '" << name << "'" << std::endl;
            exit(1);
        }

        std::shared_ptr<Object const> find(std::string const& name) const {
            auto const& property { properties.find(name) };
            if (property != properties.end()) return property->second();
            if (parent) return parent->find(name);

            std::cerr << "undefined reference to '" << name << "'" << std::endl;
            exit(1);
        }

        std::shared_ptr<Object const> call(std::function<std::shared_ptr<Object const>()> argument) const {
            if (function) return function.value()(argument);

            std::cerr << "cannot call undefined function property" << std::endl;
            exit(1);
        }

        std::shared_ptr<Object const> as_shared_ptr() const {
            return shared_from_this();
        }

        std::shared_ptr<Object const> parent;
        std::unordered_map<std::string, std::function<std::shared_ptr<Object const>()>> properties;
        std::optional<std::function<std::shared_ptr<Object const>(std::function<std::shared_ptr<Object const>()>)>> function;
    };
}
